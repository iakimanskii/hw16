#include <iostream>

int main()
{
	const int D = 31;
	const int N = 3;
	int array[N][N];
	int Sum = 0;


	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			array[i][j] = i + j;
		}
	}


	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			std::cout << array[i][j];
		}
		std::cout << "\n";
	}


	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			if (i == D % N)
			{
				Sum += i;
			}
		}
	}

	std::cout << "Sum = " << Sum;

}